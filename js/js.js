function excludeBy(arr1, arr2, foundingElement){
	let kf = [];
	for (let val of arr2){
		for (let n in val){
			if (n==foundingElement){
				kf.push(val[n]);
			}
		}
	}
	
	let uniqueObj = arr1.filter(function(obj){
		for (let i in obj){
			if (i === foundingElement){
				for (let j of kf){return obj[i] !== j} ;
			}	
		}
	})
	return uniqueObj;
}

let users = [{
  name: "Ivan",
  surname: "Ivanov",
  gender: "male",
  age: 30
},
{
    name: "Anna",
    surname: "Ivanova",
    gender: "female",
    age: 22
}]

let users2 = [{
  name: "Ivan",
  surname: "Ivanov",
  gender: "male",
  age: 30
},
{
    name: "an",
    surname: "Ivanova",
    gender: "female",
    age: 22
}];



excludeBy(users, users2, "name");